/*
Edita el archivo script.js para crear una función que haga lo siguiente:

Generar una contraseña (número entero aleatorio del 0 al 100)

Pedir al usuario que introduzca un número dentro de ese rango.

Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado;

si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.

El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido.
*/

let password = Math.floor(Math.random() * (101 - 0)) + 0;
let counter = 6;

for (let i = 5; i > 0; i--) {
  counter -= 1;
  let userPass = prompt(
    `Tienes ${counter} oportunidades de salir con vida de esta web. Introduce un numero entre 0 y 100`
  );
  if (userPass === password) {
    alert('Corre muchacho, corre antes de que me arrepienta.');
  }
  if (userPass > 100) {
    alert(
      'Has introducido un numero mayor que 100. Por favor, introduce un número entre 0 y 100.'
    );
  } else if (userPass < 0) {
    alert(
      'Has introducido un numero menor que 0. Por favor, introduce un número entre 0 y 100.'
    );
  }
}
alert('Acabas de perder tu vida virtual.');
